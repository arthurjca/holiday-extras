const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised);
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);

const mongoose = require('mongoose');

let service = require('./user/service');

const sandbox = sinon.createSandbox();

describe('User Service', () => {

  const mockUser = {
    _id: '61057c68efc38c00263f24ca',
    email: 'mock@mail.com',
    givenName: 'User',
    familyName: 'Mock',
    created: new Date()
  };

  afterEach(() => {
    sandbox.restore();
  });

  context('get all', () => {

    it('should return an array', (done) => {
      const findStub = sandbox.stub(mongoose.Model, 'find').resolves([]);
      service.getAll().then(users => {
        expect(findStub).to.have.been.calledOnce;
        expect(users).to.be.a('array');
        done();
      }).catch(err => done(err));
    });

  });

  context('get one', () => {

    it('should return a user', (done) => {

      const findByIdStub = sandbox.stub(mongoose.Model, 'findById').resolves(mockUser);
      service.getOne(123).then(user => {
        expect(findByIdStub).to.have.been.calledOnce;
        expect(findByIdStub).to.have.been.calledWith(123);
        expect(user).to.have.property('_id');
        expect(user).to.have.property('email');
        expect(user).to.have.property('givenName');
        expect(user).to.have.property('familyName');
        expect(user).to.have.property('created');
        done();
      }).catch(err => done(err));
    });

    it('should get an error for not sending ID', async () => {
      await expect(service.getOne()).to.be.rejectedWith('Missing user ID');
    });

  });

  context('create', () => {

    it('should create a user', (done) => {
      const userData = {
        email: 'mock@mail.com',
        givenName: 'User',
        familyName: 'Mock'
      };
      const createStub = sandbox.stub(mongoose.Model, 'create').resolves(mockUser);
      service.create(userData).then(user => {
        expect(createStub).to.have.been.calledOnce;
        expect(createStub).to.have.been.calledWith(userData);
        expect(user).to.be.a('object');
        expect(user).to.have.property('_id');
        expect(user).to.have.property('email');
        expect(user).to.have.property('givenName');
        expect(user).to.have.property('familyName');
        expect(user).to.have.property('created');
        done();
      }).catch(err => done(err));
    });

    it('should get an error for not sending user data', async () => {
      await expect(service.create()).to.be.rejectedWith('Missing user data');
    });

  });

  context('update', () => {

    it('should update a user', (done) => {
      const userData = { email: 'mock@mail.com' };
      const findByIdAndUpdateStub = sandbox.stub(mongoose.Model, 'findByIdAndUpdate').resolves(mockUser);
      service.update(123, userData).then(user => {
        expect(findByIdAndUpdateStub).to.have.been.calledOnce;
        expect(findByIdAndUpdateStub).to.have.been.calledWith(123, userData);
        expect(user).to.be.a('object');
        expect(user).to.have.property('_id');
        expect(user).to.have.property('email');
        expect(user).to.have.property('givenName');
        expect(user).to.have.property('familyName');
        expect(user).to.have.property('created');
        done();
      }).catch(err => done(err));
    });

    it('should get an error for not sending user ID', async () => {
      await expect(service.update()).to.be.rejectedWith('Missing user ID');
    });

    it('should get an error for not sending user data', async () => {
      await expect(service.update(123)).to.be.rejectedWith('Missing user data');
    });

  });

  context('delete', () => {

    it('should delete a user', (done) => {
      const findByIdAndDeleteStub = sandbox.stub(mongoose.Model, 'findByIdAndDelete').resolves(mockUser);
      service.deleteOne(123).then(() => {
        expect(findByIdAndDeleteStub).to.have.been.calledOnce;
        expect(findByIdAndDeleteStub).to.have.been.calledWith(123);
        done();
      }).catch(err => done(err));
    });

    it('should get an error for not sending user ID', async () => {
      await expect(service.deleteOne()).to.be.rejectedWith('Missing user ID');
    });

  });
});