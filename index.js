require("dotenv").config();
const db = require('./db');
const app = require('./app');
db.connect();
app.serve();