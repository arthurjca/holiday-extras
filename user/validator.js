const Joi = require("joi");

module.exports = {
  checkPost: async (req, res, next) => {
    try {
      const data = {
        email: req.body.email,
        givenName: req.body.givenName,
        familyName: req.body.familyName
      };
      const schema = Joi.object({
        email: Joi.string().email().required(),
        givenName: Joi.string().required(),
        familyName: Joi.string().required(),
      });
      const { error } = schema.validate(data);
      if (error) throw (error.details);
      next();
    } catch (err) {
      return res.status(500).json({ message: err.message || err });
    }
  },
  checkPut: async (req, res, next) => {
    try {
      const data = {
        email: req.body.email,
        givenName: req.body.givenName,
        familyName: req.body.familyName
      };
      const schema = Joi.object({
        email: Joi.string().email(),
        givenName: Joi.string(),
        familyName: Joi.string()
      });
      const { error } = schema.validate(data);
      if (error) throw (error.details);
      next();
    } catch (err) {
      return res.status(500).json({ message: err.message || err });
    }
  },
  checkId: async (req, res, next) => {
    if (!req.params.idUser)
      return res.status(500).json({ message: 'User ID missing' });
    next();
  }
}