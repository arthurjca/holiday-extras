const Service = require('./service');

module.exports = {
  get: async (req, res) => {
    try {
      const users = await Service.getAll();
      return res.status(200).json({
        data: { users },
        message: 'Query successful'
      });
    } catch (err) {
      console.log(err)
      return res.status(500).json({ message: err.message || err });
    }
  },
  getById: async (req, res) => {
    try {
      const user = await Service.getOne(req.params.idUser);
      if (!user) throw 'User not found';
      return res.status(200).json({
        data: { user },
        message: 'Query successful'
      });
    } catch (err) {
      return res.status(500).json({ message: err.message || err });
    }
  },
  post: async (req, res) => {
    try {
      const userData = getDataFromBody(req.body);
      let user = await Service.create(userData);
      return res.status(200).json({
        data: { user },
        message: 'User created'
      });
    } catch (err) {
      return res.status(500).json({ message: err.message || err });
    }
  },
  put: async (req, res) => {
    try {
      const userData = getDataFromBody(req.body);
      const cleanData = filterProperties(userData);
      const user = await Service.update(req.params.idUser, cleanData);
      return res.status(200).json({
        data: { user },
        message: 'User updated'
      });
    } catch (err) {
      return res.status(500).json({ message: err.message || err });
    }
  },
  deleteById: async (req, res) => {
    try {
      const user = await Service.deleteOne(req.params.idUser);
      return res.status(200).json({
        data: { user },
        message: 'User deleted'
      });
    } catch (err) {
      return res.status(500).json({ message: err.message || err });
    }
  }
}

const getDataFromBody = (body) => {
  return {
    email: body.email,
    givenName: body.givenName,
    familyName: body.familyName
  };
};

const filterProperties = (data) => {
  return Object.entries(data)
    .filter(p => p[1])
    .map(p => ({ [p[0]]: p[1] }))
    .reduce((obj, p) => Object.assign(obj, p), {});
};