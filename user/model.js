const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  // id: using Mongo id
  email: {
    type: String,
    unique: true,
    required: true
  },
  givenName: {
    type: String,
    required: true
  },
  familyName: {
    type: String,
    required: true
  },
  created: {
    type: Date,
    required: true
  }
});

module.exports = mongoose.model('User', schema);