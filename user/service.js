const User = require('./model');

module.exports = {
  getAll: async () => {
    try {
      const users = await User.find();
      return users;
    } catch (err) {
      // console.log('Error querying users:', err);
      throw new Error(err);
    }
  },
  getOne: async (_id) => {
    try {
      if (!_id) throw 'Missing user ID';
      const user = await User.findById(_id);
      if (!user) throw `User not found with ID: ${_id}`;
      return user;
    } catch (err) {
      // console.log('Error querying user:', err);
      throw new Error(err);
    }
  },
  create: async (data) => {
    try {
      if (!data) throw 'Missing user data';
      const userData = data;
      userData.created = new Date();
      const user = await User.create(userData);
      return user;
    } catch (err) {
      // console.log('Error creating user:', err);
      throw new Error(err);
    }
  },
  update: async (_id, data) => {
    try {
      if (!_id) throw 'Missing user ID';
      if (!data) throw 'Missing user data';
      const user = await User.findByIdAndUpdate(_id, data, { new: true });
      if (!user) throw `User not found with ID: ${_id}`;
      return user;
    } catch (err) {
      // console.log('Error updating user:', err);
      throw new Error(err);
    }
  },
  deleteOne: async (_id) => {
    try {
      if (!_id) throw 'Missing user ID';
      const user = await User.findByIdAndDelete(_id);
      if (!user) throw `User not found with ID: ${_id}`;
      return user;
    } catch (err) {
      // console.log('Error deleting user:', err);
      throw new Error(err);
    }
  }
}