const express = require('express');
const app = express();
const router = express.Router();

const { get, getById, post, put, deleteById } = require('./user/routes');
const { checkPost, checkPut, checkId } = require('./user/validator');

app.use(express.urlencoded({ extended: true }));
app.use(express.json())

router.get('/users', get);
router.get(['/user/', '/user/:idUser'], checkId, getById);
router.post('/user', checkPost, post);
router.put(['/user/', '/user/:idUser'], checkId, checkPut, put);
router.delete(['/user/', '/user/:idUser'], checkId, deleteById);
app.use('/api', router);

module.exports = {
  serve: () => {
    app.listen(process.env.SERVER_PORT, () => {
      console.log(`Running on port ${process.env.SERVER_PORT}`)
    });
  }
}