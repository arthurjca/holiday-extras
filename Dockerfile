FROM node:latest
RUN mkdir -p /src/holiday-extras
#setting working directory in the container
WORKDIR /src/holiday-extras
#copying the package.json file(contains dependencies) from project source dir to container dir
COPY package.json /src/holiday-extras
# installing the dependencies into the container
RUN npm install
#copying the source code of Application into the container dir
COPY . /src/holiday-extras
#container exposed network port number
EXPOSE 3000
#command to run within the container
CMD ["npm", "start"]