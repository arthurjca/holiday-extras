To run go to the project directory and execute:

**docker build -t holiday-extras-api .**

**docker-compose up**

The API will be exposed locally at port 3000.

I'm aware you shouldn't upload a .env file to git, but in this case it will make it easier for whoever evaluate this task.

I also included a Postman collection in the repo.
