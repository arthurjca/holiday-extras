const mongoose = require('mongoose');

module.exports = {
  connect: () => {
    mongoose.connect(process.env.DB_HOST, {
      useNewUrlParser: true,
      useCreateIndex: true,
      dbName: process.env.DB_NAME,
      useUnifiedTopology: true
    });
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function () {
      console.log( `Connected to ${process.env.DB_NAME} DB`);
    });
  }
};